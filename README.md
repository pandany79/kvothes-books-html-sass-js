# Kvothe's Books

## Objective.
Recreate a webpage from a mock-up and allow the users to fetch information by using the search box.

## Goals.
- The navbar should be at the very top of the page with a slightly transparent background. However, when the user scrolls down, the navbar will become opaque.
- The navbar includes 4 menu options (widescreen version).
- The webpage must be responsive.
- For the mobile & tablet versions, it must be displayed the hamburger icon to handle the menu options.
- Once the user triggers a search, it must be displayed a skeleton result with a dozen of grayed out mock results for 3 secs, while the data is fetched from the **Google Books API**.
- Once the information has been fetched and the 3 seconds has been elapsed, the mock results will be replaced with the corresponding result.
- Everytime the user commits a search, it should be stored the first genre of the results in order to be used when the user refreshes the page or if he/she uses the webpage days later.
- For the genre history, it should keep always the last 5 results.

## Steps to use it.
- Clone the repository.
- Install **SASS** preprocesor globally.
```javascript
npm install -g sass
```
- Run **SASS** to compile the corresponding **css** file.
```javascript
sass styles.sass styles.css
```
- Open the **index.html** file.
- Search for a book and click on the **Search** button or press **Enter** from your keyboard.
- Enjoy it!

## Extras.
- Added behavior for not commit a search if the search box is empty.
- Added behavior to remove unnecesary mock results if the total results is lower than 12.
- Added behavior to display an error message in case the result doesn't return any result. Also, the skeleton template will be removed.