// Get "searchField" element and add "onkeyup" event.
const searchField = document.querySelector('#searchField')
searchField.addEventListener('keyup', (e) => {
  if (e.keyCode === 13) {
    e.preventDefault()
    searchBooks()
  }
})

// Get the "results" element.
const resultsContainer = document.querySelector('#results')

// Create a genre history array from localStorage
const genreHistory = JSON.parse(localStorage.getItem('genreHistory') || '[]')

if (genreHistory.length) {
  const randomIndex = Math.floor(Math.random() * genreHistory.length)
  searchField.value = `the ${genreHistory[randomIndex]}`
  searchBooks()
}

/**
 * Create event for scrolling to update the background of the "navbar" element.
 * NOTE: When the user has scrolled more than 250px down, the "navbar" element will become opaque.
 */
window.onscroll = () => {
  const navbar = document.querySelector('#navbar')

  if (window.pageYOffset > 250) {
    navbar.classList.add('dark')
  } else {
    navbar.classList.remove('dark')
  }
};

/**
 * Toggles the navbar menu when the hamburger icon is visible.
 * NOTE: When the "navbar" element includes the "mobile" class, it means the menu is displayed.
 * When the button is triggered it adds or removes the "mobile" class to show or hide the menu.
 */
function toggleMenu() {
  const navbar = document.querySelector('#navbar')

  if (navbar.className.includes('mobile')) {
    navbar.classList.remove('mobile')
  } else {
    navbar.classList.add('mobile')
  }
}

/**
 * Creates an empty template to display content while the data is fetched.
 */
function renderTemplate() {
  // Empty the results content.
  resultsContainer.textContent = ''

  // For Loops to create the 12 elements and add them into the "results" element.
  for (let row = 0; row < 4; row++) {
    const rowContainer = document.createElement('div')
    rowContainer.id = `row${row}`
    rowContainer.className = 'row'

    for (let column = 0; column < 3; column++) {
      const thumbnail = document.createElement('img')
      thumbnail.id = `thumbnail${row}${column}`
      thumbnail.className = 'thumbnail hide'
  
      const thumbnailContainer = document.createElement('div')
      thumbnailContainer.className = 'book-thumbnail'
      thumbnailContainer.appendChild(thumbnail)
  
      const bookTitle = document.createElement('p')
      bookTitle.id = `title${row}${column}`
      bookTitle.className = 'book-title'
      bookTitle.textContent = '...............'
  
      const bookAuthor = document.createElement('p')
      bookAuthor.id = `author${row}${column}`
      bookAuthor.className = 'book-author'
      bookAuthor.textContent = 'by .........'
  
      const cardText = document.createElement('p')
      cardText.id = `cardText${row}${column}`
      cardText.className = 'card-text'
      cardText.textContent = '... '.repeat(31)
  
      const bookDetailsContainer = document.createElement('div')
      bookDetailsContainer.className = 'book-details'
      bookDetailsContainer.appendChild(bookTitle)
      bookDetailsContainer.appendChild(bookAuthor)
      bookDetailsContainer.appendChild(cardText)
  
      const bookCardContainer = document.createElement('div')
      bookCardContainer.className = 'book-card'
      bookCardContainer.appendChild(thumbnailContainer)
      bookCardContainer.appendChild(bookDetailsContainer)
  
      const cardSlotContainer = document.createElement('div')
      cardSlotContainer.id = `cardSlot${row}${column}`
      cardSlotContainer.className = 'col-12 col-lg-4 card-slot'
      cardSlotContainer.appendChild(bookCardContainer)

      // Add the new "bookCard" element into the current "row" element.
      rowContainer.appendChild(cardSlotContainer)
    }

    // Add the new "row" container into the "results" element.
    resultsContainer.appendChild(rowContainer)
  }
}

/**
 * Fetches the information from the Google Books api according with the value provided
 * through the "searchField" element.
 */
function searchBooks() {
  // Proceed with the search only if there is any value provided.
  if (searchField.value) {
    renderTemplate()
    fetch(`https://www.googleapis.com/books/v1/volumes?q=${searchField.value}&maxResults=12&printType=all&projection=full&key=AIzaSyBEJa20rNXRuQQ91LAeH5l5Zq-IRbBkH3U`)
      .then(response => response.json())
      .then(booksData => {
        const books = booksData.items
  
        // Wait for 3 seconds before display the data.
        setTimeout(() => {
          // If there is no results to display, empty the "results" element and display an error message.
          if (!books) {
            resultsContainer.textContent = ''

            const errorContainer = document.createElement('p')
            errorContainer.className = 'error'
            errorContainer.textContent = `Sorry, we couldn't find any result with the information provided!`

            resultsContainer.appendChild(errorContainer)
          } else {
            // Initialize the flag to check if a genre has been added into the genre history array.
            let genreAdded = false

            // For Loops to replace the template items content with the data fetched from the API.
            for (let row = 0; row < 4; row++) {
              const rowContainer = document.querySelector(`#row${row}`)

              for (let column = 0; column < 3; column++) {
                const index = row * 3 + column
                const book = books[index] || null
                const bookCard = document.querySelector(`#cardSlot${row}${column}`)
                const thumbnail = document.querySelector(`#thumbnail${row}${column}`)
                const bookTitle = document.querySelector(`#title${row}${column}`)
                const bookAuthor = document.querySelector(`#author${row}${column}`)
                const bookDescription = document.querySelector(`#cardText${row}${column}`)

                // If the total of books is lower than 12, remove the remaining template elements.
                if (!book) {
                  if (column > 0) {
                    bookCard.parentNode.removeChild(bookCard)
                  } else {
                    rowContainer.parentNode.removeChild(rowContainer)
                    break
                  }
                } else {
                  const { title, authors, description, imageLinks, categories } = book.volumeInfo
                  if (imageLinks) {
                    thumbnail.src = imageLinks.smallThumbnail
                    thumbnail.alt = title
                    thumbnail.classList.remove('hide')
                  }

                  bookTitle.textContent = title
                  bookAuthor.textContent = `by ${authors ? authors[0] : ''}`
                  bookDescription.textContent = description || ''
                  const bookGenre = categories ? categories[0] : undefined
        
                  // Store the first Book Genre found into the genre history array.
                  if (!genreAdded && bookGenre) {
                    genreAdded = true
        
                    // Remove the first item from the Genre History array if it has reached the maximum allowed.
                    if (genreHistory.length === 5) {
                      genreHistory.shift()
                    }
        
                    // Add the genre found.
                    genreHistory.push(categories[0])
                    localStorage.setItem('genreHistory', JSON.stringify(genreHistory))
                  }
                }
              }
            }
            for (let index = 0; index < 12; index++) {
            }
          }
        }, 3000)
      })
  }
}